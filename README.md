# Prep-Env-With-Ansible

Ce Projet consiste à la mise en place des environnements de developpement avec les pré-requis nécessaires.
En fait, j'ai utilisé Ansible pour automatiser la configuration en exploitant les roles d'Ansible-Galaxy et en cryptant les données sensibles des VMs avec Ansible-Vault.

On doit stocker le mot de passe de décryptage d'Ansible-Vault dans un fichier nommé .vault_password.txt dans GitLab en étant un Masked Variable.
GitLab va lancer Ansible avec la commande 
``` bash
ansible-playbook playbook.yml -i hosts --vault-password-file $variable_name
```
